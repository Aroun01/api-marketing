// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlPNFCB:"https://api-staging.oney.io/staging/product_catalog/v1/business_transaction?merchant_guid=FE083816AE81457D8A2070E586BC9A8C&psp_guid=07fc3f4804e54cfa922a93a404176488&payment_amount=600&business_transaction_type=PNFCB",
  urlAFFECTE:"https://api-staging.oney.io/staging/product_catalog/v1/business_transaction?merchant_guid=FE083816AE81457D8A2070E586BC9A8C&psp_guid=07fc3f4804e54cfa922a93a404176488&payment_amount=600&business_transaction_type=AFFECTE",
  urlSimulation:"https://api-staging.oney.io/staging/sale_support_tools/v1/simulation?merchant_guid=FE083816AE81457D8A2070E586BC9A8C&psp_guid=07fc3f4804e54cfa922a93a404176488&payment_amount=600",
  contentful : {

    spaceId: 'gqvjifnsisvx',
    token: 'OSIQ4ePRW3HIUiAHrf4zaVRXFvFYLGjatUI2nCMenFQ'
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
