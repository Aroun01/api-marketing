import { Component, OnInit } from '@angular/core';
import {PaiementsService} from '../../services/paiements.service';
import {ContentfulService} from '../../services/contentful.service';
import {Observable} from 'rxjs';
import { documentToHtmlString } from '@contentful/rich-text-html-renderer';
import {Paiement} from '../../model/paiement';

@Component({
  selector: 'app-detail-offre',
  templateUrl: './detail-offre.component.html',
  styleUrls: ['./detail-offre.component.css']
})
export class DetailOffreComponent implements OnInit {

  details:any;
  testCms$ : Observable<any>;
  constructor(private  paiementService : PaiementsService, private contentful:ContentfulService,) { }

  ngOnInit(): void {
    this.contentful.logContent('3CXk35Ey4Sdc8cgNTZhkKP');
    this.testCms$=this.contentful.getContent('3CXk35Ey4Sdc8cgNTZhkKP');
  }

  onActionEvent($event: Paiement) {
    this.onSimulation($event);
  }


  onSimulation(p:Paiement){
    this.paiementService.getSimulation(p).subscribe(

      data=>{
        this.details=data;
        console.log(this.details);
      }

    );
  }

  _returnHtmlFromRichText(richText) {
    if (richText === undefined || richText === null || richText.nodeType !== 'document') {
      return '<p>Error</p>';
    }

    return documentToHtmlString(richText);
  }
}
