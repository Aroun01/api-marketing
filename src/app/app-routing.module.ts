import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {DetailOffreComponent} from './detail-offre/detail-offre/detail-offre.component';

const  routes:Routes=[
  {path:"details",component:DetailOffreComponent}
];



@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
