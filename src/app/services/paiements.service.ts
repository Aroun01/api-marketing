import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Paiement} from '../model/paiement';

@Injectable({
  providedIn: 'root'
})
export class PaiementsService {



  urlPNFCB=environment.urlPNFCB;
  urlAffecte=environment.urlAFFECTE;
  urlSimulation=environment.urlSimulation;
  constructor(private http: HttpClient) { }


  getPnfcb():Observable<Paiement[]>{
    const httpHeaders= new HttpHeaders({
      'X-Oney-Authorization':'115fb9cf501847e39930d7faac1d8eb2',
      'X-Oney-Partner-Country-Code':'FR',

  });

  return this.http.get<Paiement[]>(environment.urlPNFCB,{headers:httpHeaders});

  }

  getAffecte():Observable<Paiement[]>{
    const httpHeaders= new HttpHeaders({
      'X-Oney-Authorization':'115fb9cf501847e39930d7faac1d8eb2',
      'X-Oney-Partner-Country-Code':'FR',

    });

    return this.http.get<Paiement[]>(environment.urlAFFECTE,{headers:httpHeaders});

  }

  getSimulation(p:Paiement):Observable<any>{
    const httpHeaders= new HttpHeaders({
      'X-Oney-Authorization':'115fb9cf501847e39930d7faac1d8eb2',
      'X-Oney-Partner-Country-Code':'FR',

    });

    return this.http.get<any>(environment.urlSimulation+"&business_transaction_code="+p.business_transaction_code,{headers:httpHeaders}
);
  }




}
