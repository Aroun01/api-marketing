import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PaiementsService} from '../../services/paiements.service';
import {concat, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Paiement} from '../../model/paiement';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  @Output() paiementEventEmitter:EventEmitter<any>=new EventEmitter<any>();
  paiements:Paiement[] |null =[];
  constructor(private paiementService: PaiementsService) { }

  ngOnInit(): void {
    this.onGetPaiment();
  }


  onGetPaiment(){
    concat(this.paiementService.getPnfcb(), this.paiementService.getAffecte()) .subscribe(
      data=>this.paiements.push(...data)

    )
  }

  onSelectPaiement(p:Paiement){
   this.paiementEventEmitter.emit(p);
  }

}
