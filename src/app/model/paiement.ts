export interface Paiement {

  discount_rate:number,
  business_transaction_version:number,
  minimum_selling_price:number,
  business_transaction_type:String,
  postponement_duration:number,
  customer_label:String,
  maximum_number_of_instalments:number,
  display_order:number,
  maximum_selling_price:number,
  validity_start_date:String,
  example:String,
  minimum_number_of_instalments:number,
  business_transaction_code:number,
  long_label:String,
  validity_end_date:String,
  short_label:String,
  choosable_number_of_instalments:boolean,
  free_business_transaction:boolean


}
